% copied from 2 to add multi-polymer capability

function add_polymer(atom_groups)
global inp coords elems trans_mat atom_exist bonds angles dihedrals

npol = inp.npol; nch2 = inp.nch2;

sio3_0 = [1.076 -.517 -3.726; 1.977 .799 -4.152; 2.016 -1.821 -4.099; -.192 -.61 -4.78];
sio3_1 = [.799 2.544 5.31; -.272 1.354 5.687; 2.033 2.309 6.382; 0.01447 3.928 5.7578];

ch2_1 = [.545 -.528 -1.982; -.555 -.449 -1.960; .776 -1.525 -1.562];
ch2_2 = [1.167 .559 -1.108; .935 1.557 -1.521; 2.268 .477 -1.112];


ch2_trans = [.649 .473 .353] - ch2_1(1,:); %first one is the coords of the third C
si_trans = ch2_2(1,:) - sio3_0(1,:)-ch2_trans/2;

pln = createPlane(sio3_0(1,:), ch2_trans/norm(ch2_trans)); %plane passing through Si

sio3_2 = projPointOnPlane(sio3_0,pln); %project oxygens with respect to plane passing Si to get the end SiO3
sio3_2 = sio3_2 - (sio3_0-sio3_2);
% sio3_3 = sio3_1 - (sio3_1(1,:)-sio3_0(1,:));

pol_elems = repmat({'C';'Hc';'Hc'},nch2,1);
pol_elems = [{'Sib';'Od';'Ob';'Ob'};pol_elems;{'Sib';'Od';'Ob';'Ob'}];

pol_coords = [];
for ich2 = 1:nch2
    
    pol_coords = [pol_coords;ch2_trans*(ceil(ich2/2)-1)+mod(ich2,2)*ch2_1 + ~mod(ich2,2)*ch2_2];
    
end

pol_coords = [sio3_0;pol_coords];

if mod(nch2,2)==1
    pol_coords = [pol_coords;sio3_2+2*si_trans+ch2_trans*(ceil(ich2/2)-1)];
else
    pol_coords = [pol_coords;sio3_1+ch2_trans*(ceil(ich2/2)-3)];
end

b_info = {
    {'Sib' 'C'   2    8}
    {'C'   'Hc'  2    9}
    {'C'   'C'   2    10}
    {'Sib' 'Od'  2    11}
    {'Sib' 'Ob'  2    12}
    {'Od'   'Sib' 'C'   13}
    {'Ob'   'Sib' 'C'   13}
    {'Hc'   'C'  'Hc'   14}
    {'C'    'C'  'Hc'   15}
    {'Sib'   'C' 'Hc'   16}
    {'Sib'   'C' 'C'   17}
    {'C'   'C' 'C'     18}
    {'Od'     'Sib'   'C'    'Hc'  1}
    {'Od'     'Sib'   'C'    'C'   2}
    {'Ob'     'Sib'   'C'    'Hc'  3}
    {'Ob'     'Sib'   'C'    'C'   4}
    {'Hc'     'C'     'C'    'Hc'  5}
    {'C'     'C'     'C'    'Hc'  6}
    {'Sib'   'C'     'C'    'Hc'  7}
    {'Sib'   'C'     'C'    'C'   8}
    {'C'     'C'     'C'    'C'   9}
    };

trans_mat1 = [30 0 0;0 30 0;0 0 30];

[pol_bonds,pol_angles,pol_dihedrals] = form_bonds(pol_coords,pol_elems,trans_mat1,b_info);
%

% separate two layers
% cc = trans_mat(3,:);
% lay_shift = inp.lay_shift*cc/norm(cc);
% coords(atom_groups.top_lay,:) = coords(atom_groups.top_lay,:) + lay_shift;
% trans_mat(3,:) = trans_mat(3,:) + 2*lay_shift; %2 because we have two interlayers


first_sib = [atom_groups.sib1;atom_groups.sib3];
rnd_sib = randsample(1:size(first_sib,1)/2,npol);
rnd_sib = [rnd_sib randsample(size(first_sib,1)/2+1:size(first_sib,1),npol)];

sec_sib = [atom_groups.sib4;atom_groups.sib2];

% add the polymer for each sib
tmp = [atom_groups.sib4(:,1);atom_groups.sib2(:,1)];
[coords_sib_ext,sib_idx] = box_extend(coords(tmp,:),[1:length(tmp)]',trans_mat);

%note: when lay_gap is large, the closest sib is found in sib4 for an atom
%in sib2. I have to consider sib3 and sib1 separately
pol_idx = zeros(size(pol_elems)); dihedrals = [];
for i = rnd_sib
    
    si1 = first_sib(i);
    
    dist = pnt_dist(coords(si1,:),coords_sib_ext);
    [~,idx] = min(dist); %si2 = tmp(sib_idx(idx));    
    
    pol_idx(1:4) = first_sib(i,[1 3:5]);
    pol_idx(end-3:end) = sec_sib(sib_idx(idx),[1 3:5]);    
    pol_idx(5:end-4) = length(elems)+1:length(elems)+3*nch2;
    
    tpol_coords = adjust_pol_coord(pol_coords,coords(si1,:),coords_sib_ext(idx,:),pol_elems);
    add_atoms(tpol_coords(5:end-4,:),pol_elems(5:end-4))
    
    tpol_bonds = swap_idx(pol_bonds(:,2:end),pol_idx);
    tpol_angles = swap_idx(pol_angles(:,2:end),pol_idx);
    dihedrals = [dihedrals;pol_dihedrals(:,1) swap_idx(pol_dihedrals(:,2:end),pol_idx)];
    
    od_rmv = [first_sib(i,2) sec_sib(sib_idx(idx),2)];
    bonds(any(ismember(bonds(:,3:end),od_rmv),2),:) = [];
    angles(any(ismember(angles(:,3:end),od_rmv),2),:) = [];
    atom_exist(od_rmv) = 0;
    
    bonds = [bonds;(size(bonds,1)+(1:size(tpol_bonds,1)))' pol_bonds(:,1) tpol_bonds];
    angles = [angles;size(angles,1)+(1:size(tpol_angles,1))' pol_angles(:,1) tpol_angles];
end

tbonds = bonds(:,3:end);
[~,idx] = unique(sort(tbonds,2),'rows','stable');
bonds = bonds(idx,:);

tangs = angles(:,3:end);
[~,idx] = unique(sort(tangs,2),'rows','stable');
angles = angles(idx,:);

dihedrals = [(1:size(dihedrals,1))' dihedrals];






