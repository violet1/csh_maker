%randomly choose cw, add oh - h to them

function add_cwoh
global coords elems atom_exist

nelem_layer = elems(logical(atom_exist)); %only the elements in the current model
num_si = sum(ismember(nelem_layer,{'Si','Sib'}));
num_ca = sum(ismember(nelem_layer,{'Ca','Cw'}));
ca_si_ratio = num_ca / num_si;

num_add_cw_OH = round(num_si * (ca_si_ratio - 1)); %-num_H because we are counting the OcH as CaOH
num_add_OH_top = floor(num_add_cw_OH/2);
num_add_OH_bot = num_add_cw_OH - num_add_OH_top;

%we want to add two OH to each calcium
num_cw_top = floor(num_add_OH_top/2);
num_cw_bot = floor(num_add_OH_bot/2);

mid = mean(coords); %separate Cw on top and bottom
cw_top = find(strcmp(elems,'Cw') & coords(:,3)>mid(3));
cw_bot = setdiff(find(strcmp(elems,'Cw')),cw_top);

samp_top = randsample(cw_top,num_cw_top);
samp_bot = randsample(cw_bot,num_cw_bot);

oh_unit = [0 0 0;1 0 0];

for icw = [samp_top; samp_bot]'
    elems(end+1:end+4) = {'Oh' 'H' 'Oh' 'H'};
    coords(end+1:end+2,:) = coords(icw,:) + oh_unit + [0 1 0]; %we want to put two Oh-H beside each Cw
    coords(end+1:end+2,:) = coords(icw,:) + oh_unit - [0 1 0];
    atom_exist(end+1:end+4) = 1;
end
