
function [elems, coords, trans_mat] = read_data

num_expr = '([-+])?(\d+)(\.\d*)?e([-+]?\d+)';
num_expr2 = '[+-.]?\d+[\.]?\d*';

fid = fopen ('interface_tobermorite.dat','r');
lmp = textscan(fid,'%s','delimiter','\n');
lmp = lmp{1};

idx = find(contains(lmp,'xlo'));
tmp = regexp(lmp(idx:idx+2),num_expr2, 'match');
for i = 1:numel(tmp)
    
    for j = 1:2
        box_vec(i,j) = str2num(tmp{i}{j});
    end
    
end

trans_mat = zeros(3,3);
trans_mat(1:4:end) = box_vec(:,2) - box_vec(:,1);


    

tmp = regexp(lmp{contains(lmp,'atoms')},num_expr2,'match');
natoms = str2num(tmp{1});

idx = find(contains(lmp,'Atoms'));
tmp = (lmp(idx+2:idx+natoms+1));

tmp = cellfun(@(x)regexp(x,num_expr2,'match'),tmp,'uni',0);
tmp = cell2mat(cellfun(@(x)str2num(char(x(1:10)))',tmp,'uni',0));

if size(tmp,2)>6
    
    coords = tmp(:,end-5:end-3);
else
    coords = tmp(:,end-2:end);
end
%          1    2     3     4    5    6    7    8     9    10   11
at_tps = {'Oc' 'Ca' 'SiOh' 'Od' 'Ob' 'Cw' 'Si' 'Sib' 'Ow' 'Hw' 'Oh'};
elems = arrayfun(@(x)at_tps(x),tmp(:,3));

% coords = coords - box_vec(:,1)';
% box_vec = box_vec-box_vec(:,1);

tmp = regexp(lmp{contains(lmp,'bonds')},num_expr2,'match');
if ~isempty(tmp)
    nbonds = str2num(tmp{1});
    idx = find(contains(lmp,'Bonds'));
    tmp = str2num(char((lmp(idx+2:idx+nbonds+1))));
    bonds = tmp(:,3:end);
else
    bonds = [];
    k_bond = 0;
    r0_bond = 0;
    rbonds = 0;
end

% tmp = regexp(lmp{contains(lmp,'bonds')},num_expr,'match');
% nbonds = str2num(tmp{1});
% idx = find(contains(lmp,'Bonds'));
% tmp = str2num(char((lmp(idx+2:idx+nbonds+1))));
% bonds = tmp(:,3:end);

tmp = regexp(lmp{contains(lmp,'dihedrals')},num_expr2,'match');
ndihed = str2num(tmp{1});
idx = find(contains(lmp,'Dihedrals'));
tmp = str2num(char((lmp(idx+2:idx+ndihed+1))));
diheds = tmp(:,3:end);

idx = find(contains(lmp,'Bond Coeffs'));
tmp = str2num(char((lmp(idx+2))));
if ~isempty(tmp)
k_bond = tmp(2); r0_bond = tmp(3);
end

idx = find(contains(lmp,'Dihedral Coeffs'));
tmp = str2num(char((lmp(idx+2))));
k_dihed = tmp(2);



