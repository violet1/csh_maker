function out = coord2idx(coords,atom_coords)

cnt = 1;
for ic = atom_coords'
    
    out(cnt) = find(cellfun(@(x)isequal(x,ic'),num2cell(coords,2)));
    
    cnt = cnt + 1;
    
end