function [bonds,angles,dihedrals] = form_bonds(coords,elems,trans_mat,b_info)

bond_info = b_info(cellfun(@(x)sum(cellfun(@ischar,x))==2,b_info));

bonds = [];
for ibnd = bond_info'
    ibnd = ibnd{1};
    el1_id = find(strcmp(elems,ibnd{1}));    
    el2_id = find(strcmp(elems,ibnd{2}));  
    [sec_coords_extend,el2_id_ext] = box_extend(coords(el2_id,:),el2_id,trans_mat);

    for iel = el1_id'
        dist = pnt_dist(sec_coords_extend,coords(iel,:));
        dist(dist==0) = 1000;
        tmp = el2_id_ext(dist<ibnd{3});
        
        bonds = [bonds; [repmat(ibnd{end},length(tmp),1) repmat(iel,length(tmp),1) tmp(:)]];
                
    end
    
end
tbonds = bonds(:,2:end);
[~,idx] = unique(sort(tbonds,2),'rows','stable');
bonds = bonds(idx,:);

id = rowfun2(@(x)length(unique(x))~=2,bonds(:,2:end));
bonds(cell2mat(id),:) = [];

angle_info = b_info(cellfun(@(x)sum(cellfun(@ischar,x))==3,b_info));

angles = [];
for iang = angle_info'
    iang = iang{1};
    el1_id = find(strcmp(elems,iang{2}));
    
    
    for imid = el1_id'
        
        tmp = tbonds==imid;
        catoms = tbonds(~tmp & sum(tmp,2)==1); %atoms connected to this mid atom
        sz = length(catoms);
        for iat = 1:sz
            for jat = iat+1 : sz
                angles = [angles ; iang{end} catoms(iat) imid catoms(jat)];
            end
        end
        
    end
end

tangles = angles(:,2:end);
[~,idx] = unique(sort(tangles,2),'rows','stable');
angles = angles(idx,:);

id = rowfun2(@(x)length(unique(x))~=3,angles(:,2:end));
angles(cell2mat(id),:) = [];

dihedral_info = b_info(cellfun(@(x)sum(cellfun(@ischar,x))==4,b_info));

dihedrals = [];
for idihed = dihedral_info'
    idihed = idihed{1};
    el1_id = find(strcmp(elems,idihed{1}));
    
    for i1 = el1_id'
        tmp = tbonds==i1;
        catoms1 = tbonds(~tmp & sum(tmp,2)==1);
        
        for i2 = catoms1(:)'
            tmp = tbonds==i2;
            catoms2 = tbonds(~tmp & sum(tmp,2)==1);
            
            for i3 = catoms2(:)'
                tmp = tbonds==i3;
                catoms3 = tbonds(~tmp & sum(tmp,2)==1);
                for i4 = catoms3(:)'
                    if isequal(elems([i1 i2 i3 i4])',idihed(1:4))
                        dihedrals = [dihedrals;idihed{end} i1 i2 i3 i4];
                    end
                end
                
            end
        end
    end
    
end

id = rowfun2(@(x)length(unique(x))~=4,dihedrals(:,2:end));
dihedrals(cell2mat(id),:) = [];

tdiheds = dihedrals(:,2:end);
[~,idx] = unique(sort(tdiheds,2),'rows','stable');
dihedrals = dihedrals(idx,:);

