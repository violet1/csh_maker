function pol_coords = adjust_pol_coord(pol_coords,si1,si2,pol_elems)
% global coords elems

pol_coords = pol_coords + (si1 - pol_coords(1,:));
pol_si1 = pol_coords(1,:);

mod_vec = si2 - si1; %vector connecting model's si
pol_vec = pol_coords(end-3,:) - pol_coords(1,:); %polymer vector

npvec = norm(pol_vec);
%stretch polymer
fact1 = norm(mod_vec)/npvec - 1;
pol_plane = createPlane(pol_si1,pol_vec/npvec);
for i = 1:size(pol_coords,1)
    ax_vec = distancePointPlane(pol_coords(i,:),pol_plane)*pol_vec/npvec*fact1;
    pol_coords(i,:) = pol_coords(i,:) + ax_vec;
end
pol_vec = pol_coords(end-3,:) - pol_coords(1,:); %polymer vector


ang = dot(mod_vec,pol_vec)/(norm(pol_vec)*norm(mod_vec));
rot_ax = cross(mod_vec,pol_vec);

[rot,~] = AxelRot(-acosd(ang),rot_ax/norm(rot_ax)); %I think u does not have to be a unit vector

pol_si_vec = pol_coords - si1;

%transformed polymer
pol_coords = (rot*pol_si_vec')' + si1;
