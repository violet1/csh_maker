
function defect_introduction
global coords elems atom_exist atom_groups
%% remove the bridging Si of infinite Si chain and create dimers and octamers
mat_chain = cell2mat(atom_groups.loops);
oct_dim = logical([1 1 1 0 0 1 0 0]);
pent = logical([1 0 1 0 1 0 1 0]);
nlevels = size(atom_groups.loops,1);
nchains = size(mat_chain,1);

% index for shift matrix circularly
idxRand = randi(nchains-1,nlevels,nchains/nlevels);
while idxRand(1,1)==idxRand(4,2) && idxRand(1,2)==idxRand(4,1) &&...
        idxRand(2,1)==idxRand(3,2) && idxRand(2,2)==idxRand(3,1)
    idxRand = randi(nchains-1,nlevels,nchains/nlevels);
end

for i = 1:nlevels % repeat the number of levels
    for j = 1:nchains/nlevels % each level has two Si chains
        % select four Sibs by using randomly circled matrix
        chosen_sib = atom_groups.loops{i}(j,circshift(oct_dim,idxRand(i,j)));
        idx = any(ismember(atom_groups.sib{i},chosen_sib),2); % find the selected Sib in bridging Sib group
        si_ob2 = atom_groups.sib{i}(idx,1:3); % 1:3 because we want to remove Si and two Obs
        atom_groups.ob1{i} = atom_groups.sib{i}(idx,4:5);
        atom_groups.sib1{i} = atom_groups.sib{i}(idx,1);
        atom_exist(si_ob2) = 0;
    end
end

%relationship between SiOH/Si, CaOH/Si vs Ca/Si
nelem_layer = elems(logical(atom_exist)); %only the elements in the current model
num_si = sum(ismember(nelem_layer,{'Si','Sib'}));
num_ca = sum(ismember(nelem_layer,{'Ca','Cw'}));
ca_si_ratio = num_ca / num_si;
totl_eqv_wat = (1.799*ca_si_ratio - 1.2539)*num_si; % total OH number should be in the system
molec_wat = (0.8094*ca_si_ratio - 0.5097)*num_si; % molecuar water should be in the system
n_caoh = (ca_si_ratio - 1)*num_si; % CaOH number should be in the system
n_sioh = totl_eqv_wat*2 - molec_wat*2 - n_caoh; % SiOH number should be in the system
av_ow = sum(ismember(nelem_layer,{'Ow'})); %Hw that is already in the system

add_cwoh

h2o_coord = [1.55000    1.55000    1.50000
             1.55000    2.36649    2.07736
             1.55000    0.73351    2.07736]-1; %coordinates of a single water molecule
wat_add_per_level = round((molec_wat-av_ow)/4); %how much water we need to add
for i = 1:nlevels %randomly choose the site of removed sib of each level and add a water there
    chosen_sib1 = randsample(atom_groups.sib1{i},wat_add_per_level);
    for j = 1:length(chosen_sib1)
        add_atoms(coords(chosen_sib1(j),:) + h2o_coord,{'Ow';'Hw';'Hw'})
    end
    
end

num_ob1 = nnz(cell2mat(atom_groups.ob1)); %number of ob sites that are eligible for receiving an H

% add H to Ob connected to Sid
if n_sioh <= num_ob1 
    nsioh_level = round(n_sioh/4);
    for i = 1:nlevels
        chosen_ob1 = randsample(atom_groups.ob1{i},nsioh_level); % randomly select Ob for adding H
        add_atoms(coords(chosen_ob1,: ) + [0 1 0],repmat({'H'},size(chosen_ob1)))
    end
else %when all ob sites need a hydrogen
    all_ob1 = cell2mat(atom_groups.ob1); % add H to all Ob
    add_atoms(coords(all_ob1(: ),: ) + [0 1 0],repmat({'H'},size(all_ob1(:))))
    elems(all_ob1(: )) = {'Oh'}; %since Ob is connected to H, change it to Oh
end
