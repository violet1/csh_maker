function out = swap_idx(mat,idx)
%for elements of mat equal to each label i of idx, it's replaced with the
%content of idx at that label


tmp = mat(:);
out = zeros(size(mat(:)));
for i = 1:numel(idx)
    
    out(tmp == i) = idx(i);
    
    
end

out = reshape(out,size(mat));

out(out==0) = mat(out==0);