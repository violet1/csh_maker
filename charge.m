function [vec_ch,atom_types] = charge
global elems inp

if inp.force_field == 3
    %             1    2    3    4    5    6    7    8     9    10   11  12   13
    all_atoms = {'Oc' 'Ca' 'Oh' 'Od' 'Ob' 'Cw' 'Si' 'Sib' 'Ow' 'Hw' 'H' 'Hc' 'C'};
else
    elems(ismember(elems,{'Od','Oc'})) = {'O'};
    elems(ismember(elems,{'Sib'})) = {'Si'};    %             1    2    3    4    5    6   7    8    9
    all_atoms = {'Si','Ca','Cw','Ow','Hw','H','Ob','O','Oh'};
%     {'Si','Ca','Cw','Ow','Hw','H','Ob','O','Oh'}
end


%               1     2    3    4   5    6    7   8    9    10   11   12   13
% all_atoms = {'Si','Ca','Cw','Ow','Hw','H','Ob','Oc','Od','Oh','Sib','Hc','C'};
atom_count_id = cell2mat(cellfun(@(x)strcmp(elems,x),all_atoms,'uni',0));

atom_count = sum(atom_count_id);
for i = 1:size(atom_count_id,1)
    atom_types(i) = find(atom_count_id(i,:));
end
switch inp.force_field
    case 1 %csh-ff
        %         1        2        3         4    5    6         7         8           9
        %         Si       Ca       Cw        Ow   Hw   H         Ob         O         Oh
        vec_ch = [1.722357,1.435466,1.705529,-0.82,0.41,0.288381,-1.004228,-1.192286,-1.0042280];
        
    case 2 %clay-ff
        %        {'Si','Ca', 'Cw', 'Ow',  'Hw',  'H',  'Ob',  'Oc',   'Od',  'Oh'  'Sib'  'Hc  'C'}
        vec_ch = [2.1  1.36  1.05  -0.82  0.41   .425  -1.05  -1.05   -1.05  -0.95  2.1    0    0]; %CLAYFF
        
    case 3 %interface
        %                 'Si' 'Ca','Cw','Ow', 'Hw', 'H','Ob', 'Oc', 'Od',  'Oh', 'Sib', 'Hc', 'C'
        %         vec_ch = [ 1, 1.500,1.7, -0.82,0.41, .4, -0.58,-0.94,-1.04, -0.67,  1,   0.1, -0.2];
        vec_ch = [-0.94,1.500,-0.6700,-1.04,-0.58,1.7,1 ,1,-0.82,0.41,0.4, 0.1, -0.2]; %INTERFACE-CVFF
        
end

total_charge = sum(atom_count.*vec_ch);

if inp.force_field == 1
all_o_share=total_charge/sum(atom_count(7:9));
vec_ch(7:9) = vec_ch(7:9) - all_o_share;
else
end

check_charge = sum(atom_count.*vec_ch) %last term was calculated