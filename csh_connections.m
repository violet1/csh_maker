function [bonds,angles,dihedrals] =  csh_connections
global coords elems trans_mat inp

if inp.force_field == 1 || inp.force_field == 2
    
    b_info = {{'Ow'  'Hw'  1.3  1}
        {'Oh'   'H'   1.3  1}
        {'Hw'   'Ow'  'Hw'  1.3  1}};
    
elseif inp.force_field == 3
    if inp.isrigid == 0
    b_info = {{'Oc' 'Si' 2.2  1}
        {'Oh'  'Sib'  2.2  2}
        {'Oh'  'H'   1.3  3}
        {'Sib'  'Od'  2.2  4}
        {'Si'  'Ob'  2.2  5}
        {'Sib' 'Ob'  2.2  6}
        {'Ow'  'Hw'  1.3  7}
        {'Oh'  'Si'  2.2  2}
        {'Sib' 'Oh'  'H'  1}
        {'Si' 'Oh'  'H'   1}
        {'Si'  'Ob'  'Si'  2}
        {'Si'  'Ob'  'Sib' 3}
        {'Oc'   'Si'  'Oc'  4}
        {'Oc'   'Si'  'Ob'  5}
        {'Ob'   'Si'  'Ob'  6}
        {'Oh'   'Sib' 'Oh'  7}
        {'Oh'   'Sib' 'Ob'  8}
        {'Ob'   'Sib' 'Ob'  9}
        {'Hw'   'Ow'  'Hw'  10}
        {'Oh'   'Sib' 'Od'  11}
        {'Od'   'Sib' 'Ob'  12}};
    else
            b_info = {{'Oc' 'Si' 2.2  1}
        {'Oh'  'Sib'  2.2  2}
        {'Oh'  'H'   1.3  3}
        {'Sib'  'Od'  2.2  4}
        {'Si'  'Ob'  2.2  5}
        {'Sib' 'Ob'  2.2  6}
        {'Ow'  'Hw'  1.3  7}
        {'Oh'  'Si'  2.2  2}
        {'Sib' 'Oh'  'H'  1}
        {'Si' 'Oh'  'H'   1}
        {'Si'  'Ob'  'Si'  2}
        {'Si'  'Ob'  'Sib' 3}
        {'Oc'   'Si'  'Oc'  4}
        {'Oc'   'Si'  'Ob'  5}
        {'Ob'   'Si'  'Ob'  6}
        {'Oh'   'Sib' 'Oh'  7}
        {'Oh'   'Sib' 'Ob'  8}
        {'Ob'   'Sib' 'Ob'  9}
        {'Hw'   'Ow'  'Hw'  10}
        {'Oh'   'Sib' 'Od'  11}
        {'Od'   'Sib' 'Ob'  12}
        {'Od'   'Sib' 'Ob'    'Si'  10}
        {'Oc'   'Si' 'Ob'    'Sib'  11}
        {'Sib'   'Ob' 'Si'    'Ob'  12}};
    end
end

[bonds,angles,dihedrals] = form_bonds(coords,elems,trans_mat,b_info);

bonds = [(1:size(bonds,1))' bonds];
angles = [(1:size(angles,1))' angles];
dihedrals = [(1:size(dihedrals,1))' dihedrals];
