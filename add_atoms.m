function add_atoms(coord,elem)
global coords elems atom_exist

coords = [coords;coord];
elems = [elems;elem];

atom_exist = [atom_exist;ones(size(elem))];