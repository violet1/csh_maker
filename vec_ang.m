function ang = vec_ang(varargin)
%enter d as second argument to get angle in degrees

v1 = varargin{1};
v2 = varargin{2};

ang = acos(dot(v1(:),v2(:))/norm(v1)/norm(v2));
% subspace(v1(:),v2(:));

if nargin == 3
    ang = ang*180/pi;
end

