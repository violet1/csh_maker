

function [masses,pair_coeffs,bond_coeffs,angle_coeffs,dihedral_coeffs] = force_field_params(varargin)
global bonds angles dihedrals inp



if inp.force_field == 3
    masses = [ 1  15.999400
    2  40.080002
    3  15.999400
    4  15.999400
    5  15.999400
    6  40.080002
    7  28.086000
    8  28.086000
    9  15.999400
    10   1.007970
    11   1.008000
    12   1.007970
    13   12.01115];

    pair_coeffs = [ 1   0.1400000021   2.7796039973
        2   0.2899999992   3.0290556424
        3   0.1199999991   3.0914185538
        4   0.1900000014   3.3854151269
        5   0.1599999980   3.0290556449
        6   0.2900000032   2.8954208313
        7   0.5099999995   4.0535891679
        8   0.5099999995   4.0535891679
        9   0.1554164124   3.1655200879
        10   0.0000000000   0.0000000000
        11   0.0159969724   0.9666649776
        12   0.0380000011   2.4499714540
        13   0.0389999952   3.8754094636];
    
    bond_coeffs = [1   300.0000     1.6200
        2   300.0000     1.6800
        3   495.0000     0.9450
        4   300.0000     1.5900
        5   300.0000     1.7100
        6   300.0000     1.6700
        7   540.6336     0.9600
        8   238.0000     1.8090
        9   340.6175     1.1050
        10  322.7158     1.5260]; %8-9-10: Si-C,  C-H,  C-C
    
    angle_coeffs = [ 1    50.0000   115.0000
        2   170.0000   144.0000
        3   170.0000   139.0000
        4   170.0000   112.5000
        5   170.0000   111.5000
        6   170.0000   100.2000
        7   170.0000   113.0000
        8   170.0000   111.0000
        9   170.0000   107.0000
        10    50.0000   104.5000
        11   170.0000   111.0000
        12   170.0000   107.0000
        13   44.1000   117.3000
        14   39.5000   106.4000
        15   44.4000   110.0000
        16   34.6000   112.3000
        17   34.6000   112.3000
        18   46.6000   110.5000]; %the order is in pol3.lammps
    
    dihedral_coeffs = [1    -0.1000     1     3
        2     0.1111     1     3
        3    -0.1000     1     3
        4     0.1111     1     3
        5     0.1581     1     3
        6     0.1581     1     3
        7     0.1581     1     3
        8     0.1581     1     3
        9     0.1581     1     3];
else
    masses = [ 1 28.085500
2 40.078900
3 40.078900
4 15.999400
5 1.008000
6 1.008000
7 15.999400
8 15.999400
9 15.999400];

    bond_coeffs = [1 554.134900 1.000000];
    angle_coeffs = [1 45.769600 109.470000];
    pair_coeffs = [];
    dihedral_coeffs = [];
end
% blens = varargin{1};
% angs = varargin{2};
% dhs = varargin{3};
% nd = 10;
% if nargin>0  %I want all to be rigid
%
%     [b,e1] = discretize(blens,nd);%linspace(min(dhs),max(dhs),10));
%     [a,e2] = discretize(angs,nd);%linspace(min(dhs),max(dhs),10));
%     [d,e3] = discretize(dhs+180,nd);%linspace(min(dhs),max(dhs),10));
%
%     for i = 1:length(e1)-1
%         e11(i) = mean([e1(i) e1(i+1)]);
%         e22(i) = mean([e2(i) e2(i+1)]);
%         e33(i) = mean([e3(i) e3(i+1)]);
%     end
%
%     [tmp,idx] = unique(b,'stable');
%     cblen = blens(idx);
%     tt(tmp) = 1:length(tmp);
%     b = swap_idx(b,tt);
%
%     nbnd = size(bonds,1);
%     mbt = size(bond_coeffs,1);%max num of bond types
%     for i = 1:nbnd
%         bonds = [bonds; nbnd+i mbt+b(i) bonds(i,3:end)];
%     end
%
%     ntp = length(tmp);
%     bond_coeffs = [bond_coeffs; bond_coeffs(end,1)+(1:ntp)' 9999*ones(ntp,1) ...
%         e11(tmp)'];
%
%     bond_coeffs(1:10,2) = 0;
%     angle_coeffs(1:18,2) = 0;
%
% end















