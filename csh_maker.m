

function csh_maker
clear all
global inp trans_mat coords elems atom_exist bonds angles dihedrals atom_groups

inp.Ca_Si_target = 1.0;
inp.nx = 2;inp.ny = 1;
inp.finite = 0;  %1 means finite sized
inp.remove_wat = 0;
inp.original_tob = 0; %1: don't make it near-rectangular
inp.gulmp = 2; %1:gulp 2:lammps 3:both
inp.isrigid = 0;
inp.dim_oct = 1; % Si chain type
inp.pent = 0;
inp.mjtb = 1; %use the TB from MJ?
% percentile occupied in one Si loop
% inp.dim = 20; inp.penta = 0; inp.octa = 80; 

inp.nch2 = 0; %number of carbons in the polymer
inp.npol = 0;

inp.force_field = 1; %1:csh-ff 2:clay-ff 3:interface

inp.wat_dist = -1.7; %distance of water from the surface
inp.lay_shift = 0;%inp.nch2; %gap between layers
inp.read_type = 1; %1:original tob  2:datafile

%*****************  end of input data *****************************

% test2

atom_types; 

defect_introduction

% hydrogen_adjustment

[bonds,angles,dihedrals] = csh_connections;


% [bnds,angs,dhs] = get_con_vals(bonds,angles,dihedrals);

if inp.nch2 > 0 && inp.original_tob == 0
    add_polymer(atom_groups);
end
coords = coords(logical(atom_exist),:);
elems = elems(logical(atom_exist));
bonds(:,3:end) = swap_idx(bonds(:,3:end),cumsum(atom_exist));
angles(:,3:end) = swap_idx(angles(:,3:end),cumsum(atom_exist));
dihedrals(:,3:end) = swap_idx(dihedrals(:,3:end),cumsum(atom_exist));

%% Charge manipulation
% z<17.2 and z>9.4 and not name Ob Cw Od

[vec_ch,atom_types_out] = charge;

is_close(coords,elems,.8)

make_output(vec_ch,atom_types_out)

end





