%#
function  atom_types
% atom_groups: the bridging and pairing groups that can be simply removed
% inp.nx=1;inp.ny=1;inp.read_type=1;
global inp elems coords trans_mat atom_exist atom_groups

nx = inp.nx; ny = inp.ny;

% select the transformation procedure according to the input
if inp.read_type == 1
    [elems, coords, trans_mat] = coordinate_transformation_old;
else
    [elems, coords, trans_mat] = read_data;
end

aa=trans_mat(1,:);
bb=trans_mat(2,:);
cc=trans_mat(3,:);


% % remove water
if inp.remove_wat == 1
    idx = ismember(elems,{'Ow','Hw'});
    coords(idx,:) = [];
    elems(idx)=[];
end

% detect Ob (bridging oxygen as Si-O-Si)
idx = strcmp(elems,'Si');
[extend,~] = box_extend(coords(idx,:),elems(idx,:),trans_mat);
o_id = find(strcmp(elems,'O'));
rcut = 2.3; %Si-O distance
for io = o_id' %if there is a Ca around O -> Oc and if Cw -> Od
    %distance between each O and other Si (including the Si in 26 surrounding images)
    dist = pnt_dist(extend,coords(io,:)); 
    tmp = find(dist<rcut); % if the distance is less than Si-O radius
    
    if length(tmp)==2
        elems(io) = {'Ob'}; % change atom type O -> Ob
    end
end

% Oc means oxygen in CaO layer
% number in the function is the threshold of location in z direction
if inp.mjtb == 1
    idx = (isalmost(coords(:,3), 22.04, 0.6) | isalmost(coords(:,3), 24.13, 0.6) | ...
        isalmost(coords(:,3), 12.22, 0.6) |isalmost(coords(:,3), 9.8, 0.6)|isalmost(coords(:,3), .41, 0.6))& strcmp(elems,'O');
    
else
    idx = (isalmost(coords(:,3), 20.73, 0.25) | isalmost(coords(:,3), 11.81, 0.25) | ...
        isalmost(coords(:,3), 9.35, 0.25) |isalmost(coords(:,3), 0.43, 0.25))& strcmp(elems,'O');
end

elems (idx) = {'Oc'};
idx = strcmp(elems,'O');
elems(idx) = {'Od'};

[extend,id_extend] = box_extend(coords(idx,:),find(idx),trans_mat);
si_id = find(strcmp(elems,'Si'));
rcut = 2.3; %Si-Od distance
for isi = si_id' 
    dist = pnt_dist(extend,coords(isi,:)); 
    
    if min(dist)<rcut
        elems(isi) = {'Sib'}; % change atom type O -> Ob
    end
end


if inp.mjtb == 1
ca_id = find(strcmp(elems,'Ca'));
rcut = 2.5; 
for ica = ca_id' 
    dist = pnt_dist(extend,coords(ica,:)); 
    
    if min(dist)<rcut
        elems(ica) = {'Cw'}; % change atom type O -> Ob
    end
end
end

% select the layer in the middle
if inp.mjtb == 1
    idx_bot = find(coords(:,3)<4.8);
else
idx_bot = find(coords(:,3)<3.5);
idx_bot = [idx_bot' 179 252 108 35 77 221 5 149]; 
end
% we have a complete layer in the middle, the other layer has parts near top and parts near bottom.
% Here we choose the partial layer in the bottom and "slide" it along the c dir to make one complete layer on top.
elems1 = elems(idx_bot); coords1 = coords(idx_bot,:);
elems(idx_bot) = []; coords(idx_bot,:) = [];
elems = [elems;elems1]; coords = [coords;coords1 + cc];

% constructing a n_x * n_y supercell of Tobermorite Layer
[im_a,im_b] = meshgrid(0:nx-1,0:ny-1);
im_cart = [im_a(:) im_b(:)];
dim1 = ones(1,size(im_cart,1));
im_cart = mat2cell(im_cart,dim1,2); %divide im_cart so that each cell element is the translation

% expand the fractional coordinates a and b direction
final_coords = cellfun(@(x) coords+x(1)*aa+x(2)*bb, im_cart,'uni',0);
coords = cell2mat(final_coords); % conver cell output to matrix

%duplicate the element data vertically by nx*ny
elems = repmat(elems,nx*ny,1);

% expand the transfer matrix nx and ny, respectively
trans_mat(1,:) = trans_mat(1,:) * nx;
trans_mat(2,:) = trans_mat(2,:) * ny;

atom_exist = ones(size(elems));

if inp.original_tob == 1
    atom_groups = [];
    return
end

idx1 = find(strcmp(elems,'Si'));
idx2 = find(strcmp(elems,'Sib'));
si = coords(idx1,3);
sib = coords(idx2,3);

i = 1;
while ~isempty([si;sib])
    tmp1 = isalmost(si, si(1), 1); tmp2 = isalmost(sib, sib(1), 1);
    sid_cell{i} = idx1(tmp1);
    sib_cell{i} = idx2(tmp2);
    idx1(tmp1) = [];idx2(tmp2) = [];si(tmp1) = [];sib(tmp2) = [];
    i = i + 1;
end

%replicate the box in all sides 
[extend,idx] = box_extend(coords,[1:length(elems)]',trans_mat);

% make list of connecting oxygen to brigding Si
rcut = 2.8; cnt = 1; all_sib = [sib_cell{:}]; 
for isi = all_sib(:)'
    dist = pnt_dist(extend,coords(isi,:)); % calculate the all Sib-O distance
    tmp1 = find(dist<rcut & strcmp(elems(idx),'Od')); % store Od when Sib-O < rcut
    tmp2 = find(dist<rcut & strcmp(elems(idx),'Ob')); % store Ob when Sib-O < rcut
    %make the list at each Sib
    sib_group(cnt,:) = [isi idx([tmp1;tmp2])']; %make list at each Sib
    cnt = cnt + 1;
end

%                 number of row          number of columm      
tmp = mat2cell(sib_group,length(sib_cell{1})*[1 1 1 1],size(sib_group,2)); % bre 112
atom_groups.sib = tmp; 

% make list of connecting oxygen to dimer Si
sid = [sid_cell{:}]; rcut = 2.8; cnt = 1;     
for isi = sid(:)'
    % outer for loop searches paring oxygen from dimer Si
    dist = pnt_dist(extend,coords(isi,:)); % calculate the all dimer Si-O distance
    tmp = dist<rcut & strcmp(elems(idx),'Ob'); % store Ob when Si-O < rcut
    
    % inner for loop searches the dimer Si from pairing oxygen
    for i = idx(tmp)'
        dist = pnt_dist(extend,coords(i,:)); % calculate the all Ob-Si distance
        tmp2 = idx(dist<rcut & strcmp(elems(idx),'Si')); % store Si when O-Si < rcut
        
        %the next Si found is also sid.
        %This means the Ob in the previous part connects two sid and belongs to the pairing group
        if sum(ismember(tmp2,sid))==2
            sid_pairs(cnt,:) = tmp2';
            cnt = cnt + 1;
        end
    end
end

sid_pairs = sort(sid_pairs,2); %sort the row in ascending order
% search the overlapping Ob by row and delete, but remain the order
sid_pairs = unique(sid_pairs,'rows','stable'); %remove duplicate paris

atom_groups.loops = cell(4,1);
for i = 1:4 %for each level
    
    [sid_extend,idx_sid] = box_extend(coords(sid_cell{i},:),sid_cell{i},trans_mat);
    [sib_extend,idx_sib] = box_extend(coords(sib_cell{i},:),sib_cell{i},trans_mat);
    
    while 1 %for each chain loop
        isib = sib_cell{i}(1); %start from an sib of the level (doesn't matter which)
        isib_tmp=isib;lp = isib_tmp;
        
        while 1
            [~,id] = min(pnt_dist(sid_extend,coords(isib_tmp,:)));
            chosen_sid = idx_sid(id);
            other_sid = sid_pairs(flip(chosen_sid==sid_pairs,2));
            
            [~,id] = min(pnt_dist(sib_extend,coords(other_sid,:)));
            isib_tmp = idx_sib(id);
            
            if isib==isib_tmp %the adjacent sib is the one we started with! loop is complete
                break
            end
            lp = [lp isib_tmp];
            sib_cell{i} = setdiff(sib_cell{i},lp);
        end
        atom_groups.loops{i} = [atom_groups.loops{i};lp];
        if isempty(sib_cell{i})
            break
        end
    end
end

cnt = 1;
for ipair = sid_pairs'
    tmp1 = [];tmp2 = [];
    for isi = ipair'
        dist = pnt_dist(extend,coords(isi,:));
        tmp1 = [tmp1 find(dist<rcut & strcmp(elems(idx),'Ob'))'];
        tmp2 = [tmp2 find(dist<rcut & strcmp(elems(idx),'Oc'))'];
    end
    tmp1 = unique(idx(tmp1));    
    tmp2 = idx(tmp2);
    
    sid_pairs(cnt,3:5) = tmp1; sid_pairs(cnt,6:9) = tmp2;
    cnt = cnt + 1;
end

atom_groups.sid = mat2cell(sid_pairs,length(sid_cell{1})/2*[1 1 1 1],9);

%differentiate bridging Si
si_id = find(strcmp(elems,'Si'));
od_id = strcmp(elems,'Od');
od_extend = box_extend(coords(od_id,:),elems,trans_mat);

for isi = si_id'
    dist = pnt_dist(od_extend,coords(isi,:));
    if sum(dist<2)>1
        elems(isi) = {'Sib'};
    end 
end

atom_groups.top_lay = find(coords(:,3)>16.2);
tmp = find(isalmost(coords(:,3),15.89,.1));
atom_groups.top_lay = [atom_groups.top_lay; tmp(1:2:end)];

atom_groups.bot_lay = setdiff(1:length(elems),atom_groups.top_lay)';

%increase interlayer distance
cc = trans_mat(3,:);
lay_shift = inp.lay_shift*cc/norm(cc);
coords(atom_groups.top_lay,:) = coords(atom_groups.top_lay,:) + lay_shift;
trans_mat(3,:) = trans_mat(3,:) + 2*lay_shift; %2 because we have two interlayers

fid1 = fopen('top.ndx','w+');
fprintf(fid1,'[ top ]\n');
dlmwrite('top.ndx',atom_groups.top_lay','-append','delimiter',' ')

fid1 = fopen('bot.ndx','w+');
fprintf(fid1,'[ bot ]\n');
dlmwrite('bot.ndx',atom_groups.bot_lay','-append','delimiter',' ')

end