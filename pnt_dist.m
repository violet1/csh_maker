function out = pnt_dist(a,b)
%a is a matrix of coordinates and b is coordinate of one point

out = sqrt(sum((a-b).^2,2));

