function [blen,angs,dangs] = get_con_vals(varargin)
global coords trans_mat

bonds = varargin{1};

for i = 1:size(bonds,1)
    hcoords = coords(bonds(i,4),:);
    coord_ext = box_extend(hcoords,[],trans_mat);
    dist = pnt_dist(coords(bonds(i,3),:),coord_ext);
    [~,idx] = min(dist);
    hcoords = coord_ext(idx,:);
    
    blen(i) =norm(hcoords - coords(bonds(i,3),:));
end


% blen2 = rowfun2(@(x)norm(coords(x(1),:)-coords(x(2),:)),bonds(:,3:end))';


if ismember(nargin,[2 3])
    
    angles = varargin{2};
    for i = 1:size(angles,1)
        hcoords = coords(angles(i,3:end),:);
        for j = 2:3
            coord_ext = box_extend(hcoords(j,:),[],trans_mat);
            dist = pnt_dist(hcoords(j-1,:),coord_ext);
            [~,idx] = min(dist);
            hcoords(j,:) = coord_ext(idx,:);
        end
        
        angs(i) =vec_ang((hcoords(1,:)-hcoords(2,:)),...
            (hcoords(3,:)-hcoords(2,:)),'');
    end
    %     rowfun2(@(x)vec_ang((coords(x(1),:)-coords(x(2),:)),...
    %         (coords(x(3),:)-coords(x(2),:)),''),angles(:,3:end))';
end

if nargin==3
    diheds = varargin{3};
    
    for i = 1:size(diheds,1)
        hcoords = coords(diheds(i,3:end),:);
        for j = 2:4
            coord_ext = box_extend(hcoords(j,:),[],trans_mat);
            dist = pnt_dist(hcoords(j-1,:),coord_ext);
            [~,idx] = min(dist);
            hcoords(j,:) = coord_ext(idx,:);
        end
        
        v1 = hcoords(1,:)-hcoords(2,:); v2 = hcoords(3,:)-hcoords(2,:);
        v3 = hcoords(4,:)-hcoords(2,:);
        
        thcoords = hcoords([1 2 4],:)';
        
        
        c1 = cross2(v2,v1);
        c2 = cross2(v2,v3);
        nc1 = norm(c1); nc2 = norm(c2);
        dangs(i) = acosd(c1*c2'/(nc1*nc2)) * sign(c1*(hcoords(4,:)-hcoords(3,:))');
        
        %         -distancePointPlane(hcoords(3,:), thcoords(:)')
        %         crds = coords(diheds(i,3:5),:)';
        %         n1 = planeNormal(crds(:)');
        %
        %         crds = coords(diheds(i,4:6),:)';
        %         n2 = planeNormal(crds(:)');
        %
        %         dangs(i) = vec_ang(n1,n2,'');
        
    end
    
end

